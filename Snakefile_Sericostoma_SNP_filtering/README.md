## Snakemake workflow Snakefile_Sericostoma_SNP_filtering

This workflow was used to filter genepop file generated with [stacks](http://catchenlab.life.illinois.edu/stacks) (tested for v.1.37) in a reference genome based pipeline. It requires that several individuals are used repeatedly in the RAD library for quality control. The impact of different filtering on SNP calling and on the population genetic structure is evaluated. The scripts are embedded in a [snakemake](https://snakemake.readthedocs.io) workflow.

**Program requirements**

- bash
- python3
- python package numpy
- [snakemake](https://snakemake.readthedocs.io)
- R-package [LEA](https://www.bioconductor.org/packages/release/bioc/html/LEA.html)
- R-package [optparse](https://cran.r-project.org/web/packages/optparse/index.html)
- modify_genepop.py from the same repository but not in the scripts folder

### Files

- genepop export from stacks
- catalog files from stacks
-- batch_1.catalog.snps.tsv.gz
-- batch_1.catalog.tags.tsv.gz

### Data structure

- Snakefile_Sericostoma_SNP_filtering file and Sericostoma_SNP_filtering.yaml have to be placed in the same directory together
- The two stacks catalog files have to be placed in the same directory
- The genepop file has do be placed in a subdirectory called “pop”
- All scripts have to be placed in a subdirectory called “scripts”.
- modify_genepop.py has to be placed in the scripts folder.

### Usage

Modify the snp_test.yaml file:
- set the name of the genepop file (files)
- set the batch id if not batch 1 of stacks is used (cat)
- set the minor allele frequency (maf)
- set the frequency of missing data per SNP (mds)
- set the frequency of missing data per individual (mdi)
- set the maximum number of SNPs per locus (snps)
- set the minium distance between SNPs (dist)
- set the names of the repeated individuals (pairs)
- set the ending used to identify the replicates (ending)

Run the snakemake workflow:

`snakemake -s  Snakefile_Sericostoma_SNP_filtering`
