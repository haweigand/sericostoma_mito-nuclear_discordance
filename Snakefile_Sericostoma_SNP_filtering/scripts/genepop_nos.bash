#!/usr/bin/env bash

### Written by Hannah Weigand, University of Duisburg-Essen

### This program is free software: you can redistribute it and/or modify
### it under the terms of the GNU General Public License as published by
### the Free Software Foundation, either version 3 of the License, or
### (at your option) any later version.

### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
### GNU General Public License for more details.

### You should have received a copy of the GNU General Public License
### along with this program.  If not, see <http://www.gnu.org/licenses/>.

#Get variables
genepop=$(basename $1)
cf=$2
snp=$3
out=$4
maf=$5
mds=$6
mdi=$7

#get locus & snp names
cat $1 | awk 'NR ==2' | sed 's/,/\n/g' > temp/fns/$genepop.snps
cat temp/fns/$genepop.snps | grep -Eo '^[^_]*' | sort > temp/fns/$genepop.loc

#Compare catalog and locus file
echo $cf
echo temp/fns/$genepop.loc
comm -1 -2 $cf temp/fns/$genepop.loc > temp/fns/$genepop.snps.1_$snp

#Generate Whitelist of accepted loci for modify_genepop.py
cat temp/fns/$genepop.snps.1_$snp | while read locus;
do
  cat temp/fns/$genepop.snps | grep ^$locus'_' >> temp/fns/$genepop.snps.1_$snp'.wl'
done

#Generate genepop file only with Whitelist loci
python3 scripts/modify_genepop.py -i $1 -w temp/fns/$genepop.snps.1_$snp'.wl' --maf $5 --mds $6 --mdi $7 -G $4
