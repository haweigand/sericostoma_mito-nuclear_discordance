#!/usr/bin/env python

### Written by Hannah Weigand, University of Duisburg-Essen

### This program is free software: you can redistribute it and/or modify
### it under the terms of the GNU General Public License as published by
### the Free Software Foundation, either version 3 of the License, or
### (at your option) any later version.

### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
### GNU General Public License for more details.

### You should have received a copy of the GNU General Public License
### along with this program.  If not, see <http://www.gnu.org/licenses/>.

### This program is part of a snakemake workflow


#get folder name
out_fa = snakemake.output.o1
out_lfmm = snakemake.output.o2
out1_fa = str(out_fa)
out1_lfmm = str(out_lfmm)
folder_fa = out1_fa[0:out1_fa.find("statistics")]

#Generate empty variables
locs_fa = []
locs_lfmm = []
inds_fa = []
inds_lfmm = []
name = []


#get statistics & file name
for in1 in snakemake.input:
    with open(in1) as file1:
        typ="no"
        for line in file1:
            if line.startswith("Fasta output"):
                typ="fa"
            if line.startswith("lfmm output"):
                typ="lf"
            if line.startswith("Loci exported:"):
                loc = line.rstrip("\n").split(":")[1]
                if typ == "fa":
                    locs_fa.append(loc)
                elif typ == "lf":
                    locs_lfmm.append(loc)
            if line.startswith("Individuals exported:"):
                ind = line.rstrip("\n").split(":")[1]
                if typ == "fa":
                    inds_fa.append(ind)
                elif typ == "lf":
                    inds_lfmm.append(ind)
    name1 = str(in1)[len(folder_fa):in1.find(".log")].split("_")
    name.append(name1)

#Generate output
output_fa = open(out_fa, "w")
output_fa = open(out_fa, "a")
output_fa.write("name\tmaf\tmds\tmdi\tmax_snps\tdist\tbatch\tloci\tinds")
for pos in range(len(name)):
    output_fa.write("\n" + name[pos][0] + "\t" + name[pos][1][3:] + "\t" + name[pos][2][3:] + "\t" + name[pos][3][3:] + "\t"+ name[pos][4][4:] + "\t"+ name[pos][5][4:] + "\t" + name[pos][6][1:] + "\t")
    output_fa.write(locs_fa[pos] + "\t")
    output_fa.write(inds_fa[pos])
output_fa.close()

output_lfmm = open(out_lfmm, "w")
output_lfmm = open(out_lfmm, "a")
output_lfmm.write("name\tmaf\tmds\tmdi\tmax_snps\tdist\tbatch\tloci\tinds")
for pos in range(len(name)):
    output_lfmm.write("\n" + name[pos][0] + "\t" + name[pos][1][3:] + "\t" + name[pos][2][3:] + "\t" + name[pos][3][3:] + "\t"+ name[pos][4][4:] + "\t"+ name[pos][5][4:] + "\t" + name[pos][6][1:] + "\t")
    output_lfmm.write(locs_lfmm[pos] + "\t")
    output_lfmm.write(inds_lfmm[pos])
output_lfmm.close()
