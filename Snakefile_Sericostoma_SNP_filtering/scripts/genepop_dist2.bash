#!/usr/bin/env bash

### Written by Hannah Weigand, University of Duisburg-Essen

### This program is free software: you can redistribute it and/or modify
### it under the terms of the GNU General Public License as published by
### the Free Software Foundation, either version 3 of the License, or
### (at your option) any later version.

### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
### GNU General Public License for more details.

### You should have received a copy of the GNU General Public License
### along with this program.  If not, see <http://www.gnu.org/licenses/>.

#Get variables
inp_l=$1
genepop=$(basename $2)
dist=$3
out=$4
maf=$5
mds=$6
mdi=$7

#Use snps_location.py to generate Whitelist of accepted loci for modify_genepop.py
python scripts/snps_location.py $inp_l temp/fd/$genepop.snps temp/fd/$genepop.$3.wl -l $3

#Generate genepop file only with Whitelist loci
python scripts/modify_genepop.py -i $2 -w temp/fd/$genepop.$3.wl --maf $5 --mds $6 --mdi $7 -G $4
