#!/usr/bin/env bash

### Written by Hannah Weigand, University of Duisburg-Essen

### This program is free software: you can redistribute it and/or modify
### it under the terms of the GNU General Public License as published by
### the Free Software Foundation, either version 3 of the License, or
### (at your option) any later version.

### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
### GNU General Public License for more details.

### You should have received a copy of the GNU General Public License
### along with this program.  If not, see <http://www.gnu.org/licenses/>.

#Get variables
genepop=$(basename $1)
ca=$2
out=$3


#Get loci and SNP names
cat $1 | awk 'NR ==2' | sed 's/,/\n/g' | sed "s/_/\t/g" | sort > temp/fd/$genepop.snps
cat temp/fd/$genepop.snps | awk '{print $1}' | sort | uniq > temp/fd/$genepop.locus

#Get positions of loci
cat temp/fd/$genepop.locus | while read locus;
do
  cat $ca | awk -v lo=$locus '{OFS="\t"} {if ($1 == lo) print}' >> $out
done
