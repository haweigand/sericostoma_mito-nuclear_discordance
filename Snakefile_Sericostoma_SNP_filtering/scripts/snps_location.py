#!/usr/bin/env python

### Written by Hannah Weigand, University of Duisburg-Essen

### This program is free software: you can redistribute it and/or modify
### it under the terms of the GNU General Public License as published by
### the Free Software Foundation, either version 3 of the License, or
### (at your option) any later version.

### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
### GNU General Public License for more details.

### You should have received a copy of the GNU General Public License
### along with this program.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import division
import sys
import argparse
from argparse import RawTextHelpFormatter

#Function section
#Load locus information
def read_locus_data(my_data):
    i = 0
    locus_list = []
    with open(my_data) as file1:
        for line in file1:
            i = i + 1
            line2 = line.rstrip("\n")
            if i == 1:
                heading = line2
            else:
                locus_list.append(line2)

    return(heading, locus_list)

#Load SNP positions
def read_snp_data(my_data):
    pos_dic = {}
    with open(my_data) as file1:
        for line in file1:
            line2 = line.rstrip("\n").split("\t")
            loc_inf = pos_dic.get(int(line2[0]),[])
            loc_inf.append(int(line2[1]))
            pos_dic[int(line2[0])] = loc_inf
    return(pos_dic)

#Extract Locus information
def infos(locus_list):
    locus_infos = {}
    for locus in locus_list:
        loc = locus.split("\t")
        id1 = int(loc[0])
        scaf = loc[1]
        pos = int(loc[2])
        direc = loc[3]
        seq_len = len(loc[4])
        info = locus_infos.get(scaf,[])
        info.append([pos,direc,seq_len,id1])
        locus_infos[scaf] = info

    return(locus_infos)

#Test which SNPs are in a adequate distance if several SNPs are present at one Scaffold
def validation(validate,accepted_ids, accepted_snps,limit):
    for sca,lists in validate.items():
        ids = lists[0]
        snps = lists[1]
        positions = lists[2]
        ids_sorted = [id1 for (pos,id1) in sorted(zip(positions,ids))]
        snps_sorted = [snp for (pos,snp) in sorted(zip(positions,snps))]
        positions_sorted = sorted(positions)
        comp = int(positions_sorted[0])
        accepted_ids.append(ids_sorted[0])
        accepted_snps.append(snps_sorted[0])
        for pos in range(1,len(positions_sorted)):
            if int(positions_sorted[pos]) > (comp + int(limit)):
                accepted_ids.append(ids_sorted[pos])
                accepted_snps.append(snps_sorted[pos])
                comp = int(positions_sorted[pos])

    return(accepted_ids,accepted_snps)

#Test all scaffold if the SNPs are in a adequate distance
def test_scaffolds(locus_infos, pos_dic):
    accepted_ids = []
    accepted_snps = []
    validate = {}
    for sca, info in locus_infos.items():
        if len(info) == 1:
            accepted_ids.append(info[0][3])
            accepted_snps.append(pos_dic.get(info[0][3])[0])
        if len(info)> 1:
            ids = []
            snps = []
            positions = []
            for locus in info:
                snp_list = pos_dic.get(locus[3])
                for snp in snp_list:
                    ids.append(locus[3])
                    snps.append(snp)
                    if locus[1] == "+":
                        positions.append(int(locus[0]) + int(snp))
                    else:
                        positions.append(int(locus[0]) - int(snp))

            validate[sca] = [ids,snps,positions]

    accepted_ids,accepted_snps = validation(validate,accepted_ids,accepted_snps,limit)

    return(accepted_ids,accepted_snps)

#Generate output
def generate_output(accepted_ids,accepted_snps,my_data,sym):
    output = open(my_data, "w")
    output = open(my_data, "a")
    accepted = [(id1,snp) for (id1,snp) in sorted(zip(accepted_ids,accepted_snps))]
    for loc in accepted[:-1]:
        output.write(str(loc[0]) + str(sym) + str(loc[1]) + "\n")
    output.write(str(accepted[-1][0]) + str(sym) + str(accepted[-1][1]))
    output.close()

#Define command-line variables
parser = argparse.ArgumentParser(description = "Formating and filtering of genepop files", formatter_class=RawTextHelpFormatter)

parser.add_argument('input1',
    help="input file from genepop_distance_snps.bash with locus information data")

parser.add_argument('input2',
    help="input file from genepop_distance_snps.bash with locus Id and position of SNPs")

parser.add_argument('output',
    help="output file for further processing in genepop_distance_snps.bash")

parser.add_argument('-l', '--limit',
    nargs="?",
    type=int,
    default=1000,
    help="Only consider SNPs with a minimal distance of this value\nDefault: 1000")

parser.add_argument('-s', '--separator',
    nargs="?",
    type=str,
    default="_",
    help="Separator for Locus ID and position in the output file")

args = parser.parse_args()

#Store command-line variables
input1 = args.input1
input2 = args.input2
output = args.output
limit = args.limit
sep = args.separator

#Run the program
heading, locus_list = read_locus_data(input1)
pos_dic = read_snp_data(input2)
locus_infos = infos(locus_list)
accepted_ids,accepted_snps = test_scaffolds(locus_infos,pos_dic)
generate_output(accepted_ids,accepted_snps,output,sep)
