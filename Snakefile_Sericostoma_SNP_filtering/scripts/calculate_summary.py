#!/usr/bin/env python

### Written by Hannah Weigand, University of Duisburg-Essen

### This program is free software: you can redistribute it and/or modify
### it under the terms of the GNU General Public License as published by
### the Free Software Foundation, either version 3 of the License, or
### (at your option) any later version.

### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
### GNU General Public License for more details.

### You should have received a copy of the GNU General Public License
### along with this program.  If not, see <http://www.gnu.org/licenses/>.

### This program is part of a snakemake workflow


#get folder name
out = snakemake.output
out1 = str(out)
folder= out1[0:out1.find("statistics")]

#Generate empty variables
locs = []
inds = []
name = []

#get statistics & file name
for in1 in snakemake.input:
    with open(in1) as file1:
        for line in file1:
            if line.startswith("Loci exported:"):
                loc = line.rstrip("\n").split(":")[1]
                locs.append(loc)
            if line.startswith("Individuals exported:"):
                ind = line.rstrip("\n").split(":")[1]
                inds.append(ind)
    name1 = str(in1)[len(folder):in1.find(".log")].split("_")
    print(name1)
    name.append(name1)

#Generate output
output_s = open(out[0], "w")
output_s = open(out[0], "a")

if len(name[0]) == 3:
    output_s.write("name\tmaf\tmds\tloci\tinds")
    for pos in range(len(name)):
        output_s.write("\n" + name[pos][0] + "\t" + name[pos][1][3:] + "\t" + name[pos][2][3:] + "\t" )
        output_s.write(locs[pos] + "\t")
        output_s.write(inds[pos])


elif len(name[0]) == 5:
    output_s.write("name\tmaf\tmds\tmax_snps\tbatch\tloci\tinds")
    for pos in range(len(name)):
        output_s.write("\n" + name[pos][0] + "\t" + name[pos][1][3:] + "\t" + name[pos][2][3:] + "\t" + name[pos][3][4:] + "\t" + name[pos][4][1:] + "\t")
        output_s.write(locs[pos] + "\t")
        output_s.write(inds[pos])

elif len(name[0]) == 7:
    output_s.write("name\tmaf\tmds\tmdi\tmax_snps\tdist\tbatch\tloci\tinds")
    for pos in range(len(name)):
        output_s.write("\n" + name[pos][0] + "\t" + name[pos][1][3:] + "\t" + name[pos][2][3:] + "\t" + name[pos][3][3:] + "\t"+ name[pos][4][4:] + "\t"+ name[pos][5][4:] + "\t" + name[pos][6][1:] + "\t")
        output_s.write(locs[pos] + "\t")
        output_s.write(inds[pos])

else:
    output_s.write("name\tloci\tinds")
    name2 = ""
    for na in name:
	name2 = name2 + na 
        output_s.write("\n" + name2 + "\t")
        output_s.write(locs[pos] + "\t")
        output_s.write(inds[pos])

output_s.close()
