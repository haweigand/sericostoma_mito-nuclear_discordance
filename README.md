# What is this repository for

This repository includes scripts used in the publication Weigand H, Weiss M, Cai H, Li Y, Zhang C & Leese F (2017) Deciphering the origin of mito-nuclear discordance in two sibling caddisfly species.

## Snakemake workflow Snakefile_Sericostoma_SNP_filtering

This workflow was used to filter genepop file generated with [stacks](http://catchenlab.life.illinois.edu/stacks) (tested for v.1.37) in a reference genome based pipeline. It requires that several individuals are used repeatedly in the RAD library for quality control. The impact of different filtering on SNP calling and on the population genetic structure is evaluated. The scripts are embedded in a [snakemake](https://snakemake.readthedocs.io) workflow. For details please refere to the Readme file in the folder.

## modify_genepop

This folder includes the script modify_genepop.py to filter and convert genepop files.
Additionally, a folder called validation is included which can be used to validate the functionality of modify_genepop.py
The script is ready-to-use on command line. For details please refere to the Readme file in the folder.
