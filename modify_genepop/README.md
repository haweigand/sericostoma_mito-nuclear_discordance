##What is this folder for

This folder includes the script modify_genepop.py to filter and convert genepop files.
Additionally, a folder called validation is included which can be used to validate the functionality of modify_genepop.py
The script is ready-to-use on command line.

**Requirements**

- python2 or python3
- python package numpy
- python package nose

##Description

Different filtering options can be used in the program and are listed here according to their application to the data:

- Individuals can be excluded or assigned to new populations using a population assignment file. Only those individuals with an assigned population (can be similar to the old population) in this file are used for the further reformatting. Individuals in the file but without population as well as individuals not included in the file are excluded from the further reformatting. Individuals present at the list but missing from the input file are ignored. If no population assignment file is provided, all individuals with their original populations will be used.

- A Whitelist file can be used. Only loci present in this file are used for the further reformatting. Loci present at the list put not the input file are ignored.

- A Blacklist file can be used. Only loci absent from this list are used for the further reformatting. Loci present at the list put not the input file are ignored.

- The script can be used to filter SNPs by
    * minor allele frequency.
    * missing data frequency.
    * exclusion of SNPs which are fixed in all individuals (homozygote and heterozyoge).
    * exclusion of SNPs with more than two alleles.


- Individuals can be filtered by missing data frequency.

- If SNPs as well as Individuals are filtered, the filtering will be done repeatedly until all SNPs and individuals fulfil the requirements. SNPs are filtered in a first step followed by the filtering of the individuals. If the SNP requirements are not fulfilled after exclusion of low quality individuals, the filtering is repeated, starting with the SNPs.

- Restrict data to the first valid SNP per locus

- Three different output files can be generated:
    * genepop format
    * fasta format
    * lfmm format (exclusion of fixed SNPs and SNPs with more than two alleles to satisfy the Lfmm requirements)

##Help message with detailed information

usage:

`modify_genepop.py  [-h] [-i [INPUT]] [-I] [--maf [MAF]] [--mds [MDS]]
                         [--mdi [MDI]] [--fix] [--mta] [--single] [-p [POP]]
                         [-w [WHITELIST]] [-b [BLACKLIST]] [-s [SLP1]]
                         [--slp2 [SLP2]] [--slp3 [SLP3]] [-G [GENEPOP]]
                         [-L [LFMM]] [-F [FASTA]] [-f [FSTAT]] [-B [BAYESCAN]]
                         [--isp [ISP]] [-c CODE CODE CODE CODE] [-V]`

Formatting and filtering of genepop files

optional arguments:

- -h, --help: show this help message and exit
- -i, --input [INPUT]: input file in genepop format
- -I, --info : generates a file including input parameters and output overview; Default: information is written to standard output
- --maf [MAF]: minor allele frequency per SNP; Default: 0
- --mds [MDS]: maximum percentage of missing data per SNP; Default: 1
- --mdi [MDI]: maximum percentage of missing data per Individual; Default: 1
- --fix: exclude fixed SNPs; Default: not excluded
- --mta: exclude SNPs with more than two alleles; Default: not excluded
- --single: if multiple SNPs are included per locus, use only first SNP; Default: all SNPs per locus used
- -p, --pop [POP]: file to assign new populations; Individuals not assigned to any population or not included in the file are excluded from the further analysis; Format: One line per individual: Individual name;tab or whitespace;population; Default: no assignment of new populations
- -w, --whitelist [WHITELIST]; Only consider SNPs in this file; Format: One line per SNP: SNP name;locus position separator3;population; Default: no Whitelist
- -b, --blacklist [BLACKLIST]; Exclude all SNPs from this File from the dataset; Format: One line per SNP: SNP name;locus position separator3;population; Default: no Blacklist
- -s, --slp1 [SLP1]: separator among locus and position; separator for input file; if slp2 is not defined also separator for output file;                       Default: '_'
- --slp2 [SLP2]: separator among locus and position; separator for output file; if slp3 is not defined also separator Whitelist/Blacklist
- --slp3 [SLP3]: separator among locus and position; separator for Whitelist/Blacklist
- -G, --genepop [GENEPOP]: generate genepop output; Default: not generated
- -L, --lfmm [LFMM]: generate LFMM output; Default: not generated
- -F, --fasta [FASTA]: generate fasta output; Default: not generated
- -f, --fstat [FSTAT]: generate fstat output; Default: not generated
- -B, --bayescan[BAYESCAN]: generate bayescan output; Default: not generated
- --isp [ISP]: separator among individual and loci in genepop format; Default: ' ,  '
- -c CODE CODE CODE CODE, --code CODE CODE CODE CODE: use alternative genepop code for a, c, g, t; Default: 01 02 03 04
- -V, --validate: Validate the functionality of the program
